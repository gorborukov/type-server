class ApiKeysController < ApplicationController
  before_action :authenticate_user!

  def index
    @user = User.find(params[:user_id])
    @api_key = User.apikeys
  end

  def create
    @user = User.find(params[:user_id])
    @api_key = ApiKey.new(create_new_api_key)
    create_api_key(@api_key, @user)
    end

  def destroy
    @user = User.find(params[:user_id])
    destroy_api_key(@user)
  end

  private

  def create_new_api_key
    params.permit(:api_key, user_attributes: [:id])
  end
end