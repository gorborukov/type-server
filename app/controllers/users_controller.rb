class UsersController < ApplicationController

  def create

  end

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:phone, :email, :pin, :password, :current_password, :xmpp_password)
    end

end