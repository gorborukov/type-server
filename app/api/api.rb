require 'grape'
require 'xmpp4r'
require 'xmpp4r/muc'
require 'xmpp4r/roster'
require 'xmpp4r/client'
require 'securerandom'
include Jabber


class API < Grape::API
  prefix 'api'
  default_error_formatter :json
  rescue_from :all, :backtrace => true
  
  mount Register
  mount Messages
  helpers do  
    def authenticate!
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
    end

    def current_user
      # find token. Check if valid.
      token = ApiKey.where(access_token: params[:token]).first
      if token && !token.expired?
        @current_user = User.find(token.user_id)
      else
        false
      end
    end
  end  
end