class Messages < Grape::API
  format :json
  version 'v1', :using => :path
  
  helpers do
    def current_user
      # find token. Check if valid.
      token = ApiKey.where(access_token: params[:token]).first
      if token && !token.expired?
        @current_user = User.find(token.user_id)
      else
        false
      end
    end
  end

  resources :interface do
    params do
      requires :phone, type: String, desc: "User phone"
      requires :token, type: String, desc: "Auth token"
      requires :destination, type: String, desc: "Destination user email address"
      requires :message, type: String, desc: "Message body"
    end
  
    post :send do
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
      client = Jabber::Client.new(Jabber::JID.new(current_user.email))
      client.connect
      client.auth('#{xmpp_password}')
      message = Jabber::Message::new(params[:destination], params[:message])
      message.set_type(:chat)
      Jabber::debug = true
      client.send message
      {message: 'Message successfully sent'}
    end

    get :recieve do
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
      Jabber::debug = true
      client = Jabber::Client.new(Jabber::JID.new(current_user.email))
      client.connect
      client.auth('#{xmpp_password}')
      client.send(Jabber::Presence.new.set_show(nil))
      client.add_message_callback do |message|
        if message.type != :error
          puts message.body
        end
	  end
    end
  end
end