class Register < Grape::API
  format :json
  version 'v1', :using => :path
  
  service = 'typechat.cloudapp.net'

  helpers do
    def current_user
      # find token. Check if valid.
      token = ApiKey.where(access_token: params[:token]).first
      if token && !token.expired?
        @current_user = User.find(token.user_id)
      else
        false
      end
    end
  end

  resources :auth do

    params do
      requires :phone, type: String, desc: "User phone"
    end
  
    post :register do
      phone_number = params[:phone]
      email = params[:phone] + '@'+ service
      password = Devise.friendly_token.first(8)
      pin = 1234#Devise.friendly_token.first(4)
      xmpp_password = Devise.friendly_token.first(8)
      user = User.create!(phone: phone_number, email: email, password: password, pin: pin, xmpp_password: xmpp_password) unless user.present?
      if user.save
        client = Jabber::Client.new(Jabber::JID.new(user.email))
        client.connect
        client.register('#{xmpp_password}')
        {message: 'Well'}
      else
        {message: 'Fail'}
      end
    end
    
=begin
    get :confirm do
      user = User.where(phone: params[:phone]).first
      # user.pin to sms
    end
=end

    post :signin do
      user = User.where(phone: params[:phone]).first
      if user.pin = params[:pin]
        key = ApiKey.create(user_id: user.id)
        {token: key.access_token}
        client = Jabber::Client.new(Jabber::JID.new(user.email))
        client.connect
        Jabber::debug = true
        client.auth('#{xmpp_password}')
      else
        error!('Unauthorized.', 401)
      end
    end

    params do
      requires :token, type: String, desc: "Access token."
    end

    post :connect do
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
      client = Jabber::Client.new(Jabber::JID.new(current_user.email))
      client.connect
      Jabber::debug = true
      client.auth('#{xmpp_password}')
      {message: 'Connected'}
    end

    get :ping do
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
      { message: "so good" }
      current_user.email
      encrypt(current_user.password)
    end

    post :signout do
      error!('Unauthorized. Invalid or expired token.', 401) unless current_user
      #destroy token with current_user id
    end

  end
end