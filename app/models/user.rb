class User < ActiveRecord::Base
  attr_accessor :unhashed_password
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  NUM_REGEX = /\A[0-9]+\z/
  validates :phone, presence: true
  validates :phone, format: {with: NUM_REGEX }
  validates :phone, uniqueness: true
end
